﻿//using Microsoft.Web.Administration;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;

namespace XMLMerging
{
    public partial class Form1 : Form
    {
        List<string> templateXpathList;
        string templateConfig;
        string actualConfig;

        List<string> xpathExclusionList = new List<string> {
            "configuration", "configSections", "system.web.webPages.razor", "system.transactions", "system.web",
            "system.webServer", "runtime", "system.data", "assemblyBinding", "dependentAssembly", "DbProviderFactories",
            "tracing", "traceFailedRequests", "rewrite", "rules", "requestFiltering", "security", "customHeaders",
            "httpProtocol", "staticContent", "namespaces", "pages", "assemblies", "buildProviders"
        };

        List<string> usersSpecificXpathList = new List<string>();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            templateXpathList = new List<string>();
            templateConfig = TemplateFilePath.Text.Trim();
            actualConfig = ActualFilePath.Text.Trim();

            usersSpecificXpathList = ConfigExclusionList.Text.Split(',').ToList();

            XmlTextReader templateConfigReader = new XmlTextReader(templateConfig);
            templateConfigReader.WhitespaceHandling = WhitespaceHandling.None;
            XDocument templateDoc = XDocument.Load(templateConfigReader);

            XmlTextReader actualConfigReader = new XmlTextReader(actualConfig);
            actualConfigReader.WhitespaceHandling = WhitespaceHandling.None;
            XDocument actualDoc = XDocument.Load(actualConfigReader);

            GetAllXPaths(templateDoc.Root); // Collect all the xpath as per the web.config

            XmlNamespaceManager nsManager = new XmlNamespaceManager(templateConfigReader.NameTable);
            nsManager.AddNamespace("prefix", "urn:schemas-microsoft-com:asm.v1");

            foreach (string xpath in templateXpathList)
            {
                XElement templateSelectedElement, selectedElement;
                if (xpathExclusionList.Any(x => xpath.EndsWith(x)))
                    continue;

                if (usersSpecificXpathList.Any(x => xpath.Contains(x.Trim())))
                    continue;

                if (xpath.Contains("assemblyIdentity"))
                {
                    templateSelectedElement = templateDoc.XPathSelectElement(xpath.Trim(), nsManager);
                    templateSelectedElement = templateSelectedElement.Parent;
                }
                else
                {
                    templateSelectedElement = templateDoc.XPathSelectElement(xpath.Trim(), nsManager);
                }

                if (templateSelectedElement.Parent == null) // root node dont have any parent
                {
                    continue;
                }

                if (xpath.Contains("assemblyIdentity"))
                {
                    selectedElement = actualDoc.XPathSelectElement(xpath.Trim(), nsManager);
                    if (selectedElement != null)
                        selectedElement = selectedElement.Parent;
                }
                else
                {
                    selectedElement = actualDoc.XPathSelectElement(xpath.Trim(), nsManager);
                }

                if (selectedElement != null)
                {
                    if (selectedElement.Parent == null) // root node do not have a parent
                    {
                        continue;
                    }
                    else
                    {
                        if (xpath.Contains("httpRuntime"))
                        {

                        }

                        // Compare the nodes, if the nodes are different, replace the node as it is from template file, else skip it.
                        if (!XElement.DeepEquals(templateSelectedElement, selectedElement))
                        {
                            selectedElement.ReplaceWith(templateSelectedElement);
                        }
                    }
                }
                else
                {
                    // Add the node under its respective parent as is from template file
                    string parentElementXpath = (templateSelectedElement.Parent).GetAbsoluteXPath();
                    var parentElement = actualDoc.XPathSelectElement(parentElementXpath.Trim(), nsManager);
                    parentElement.Add(templateSelectedElement);
                }
            }

            templateConfigReader.Close();
            actualConfigReader.Close();

            actualDoc.Save(actualConfig, SaveOptions.OmitDuplicateNamespaces);

            MessageBox.Show("Successfully Merged");
        }

        void GetAllXPaths(XElement element)
        {
            //textBox1.Text = textBox1.Text + element.GetAbsoluteXPath() + Environment.NewLine;
            templateXpathList.Add(element.GetAbsoluteXPath());

            foreach (XElement child in element.Elements())
            {
                GetAllXPaths(child);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            openFileDialog1.ShowDialog();
        }

        private void openFileDialog1_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            TemplateFilePath.Text = openFileDialog1.FileName;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            openFileDialog2.ShowDialog();
        }

        private void openFileDialog2_FileOk(object sender, System.ComponentModel.CancelEventArgs e)
        {
            ActualFilePath.Text = openFileDialog2.FileName;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            XmlDocument portalTempalteConfig = new XmlDocument();
            portalTempalteConfig.Load(@"C:\Users\ramkumar.bv\Desktop\ConfigMergingTesting\web.template.config");
            XmlNamespaceManager xmlNamespaceManager1 = new XmlNamespaceManager(portalTempalteConfig.NameTable);

            XmlNode templateCustomHeaders = portalTempalteConfig.DocumentElement.SelectSingleNode(@"/configuration/system.webServer/httpProtocol/customHeaders", xmlNamespaceManager1);


            XmlDocument portalConfig = new XmlDocument();
            portalConfig.Load(@"C:\Users\ramkumar.bv\Desktop\ConfigMergingTesting\v8.0-web.config");
            XmlNamespaceManager xmlNamespaceManager2 = new XmlNamespaceManager(portalConfig.NameTable);

            XmlNode customHeaders = portalConfig.DocumentElement.SelectSingleNode(@"/configuration/system.webServer/httpProtocol/customHeaders", xmlNamespaceManager2);
            if (customHeaders != null)
            {
                foreach (XmlNode chNode in customHeaders.ChildNodes)
                {
                    XmlNode xnd = templateCustomHeaders.SelectSingleNode(string.Format(@"{0}[@{1}='{2}']", chNode.Name, chNode.Attributes[0].Name, chNode.Attributes[0].Value));
                    if (xnd == null)
                    {
                        XmlNode newNode = portalTempalteConfig.CreateNode(XmlNodeType.Element, chNode.Name, String.Empty);

                        foreach (XmlAttribute attr in chNode.Attributes)
                        {
                            XmlAttribute newAtt = portalTempalteConfig.CreateAttribute(attr.Name);
                            newAtt.Value = attr.Value;
                            newNode.Attributes.Append(newAtt);
                        }

                        templateCustomHeaders.AppendChild(newNode);
                    }
                }


                XmlNode xpwdbyNode = customHeaders.SelectSingleNode(@"remove[@name='X-Powered-By']");
                if (xpwdbyNode == null)
                {
                    //<remove name="X-Powered-By" />
                    XmlNode removeNode = portalConfig.CreateNode(XmlNodeType.Element, "remove", String.Empty);

                    XmlAttribute nameAttr = portalConfig.CreateAttribute("name");
                    nameAttr.Value = @"X-Powered-By";

                    removeNode.Attributes.Append(nameAttr);
                    customHeaders.AppendChild(removeNode);
                }
            }


            //ExeConfigurationFileMap fileMap = new ExeConfigurationFileMap { ExeConfigFilename = @"C:\Users\ramkumar.bv\Desktop\ConfigMergingTesting\v8.0-web.config" };
            //Configuration configuration = ConfigurationManager.OpenMappedExeConfiguration(fileMap, ConfigurationUserLevel.None);

            //System.Web.Configuration.HttpRuntimeSection httpRuntimeSection = (System.Web.Configuration.HttpRuntimeSection)configuration.GetSection("system.web/httpRuntime");
            //httpRuntimeSection.EnableVersionHeader = true;

            //ConfigurationSection cs = configuration.GetSection("system.webServer");
            //if (cs != null)
            //{
            //    XDocument xml = XDocument.Load(new StringReader(cs.SectionInformation.GetRawXml()));
            //    //XmlNamespaceManager xmlNamespaceManager = new XmlNamespaceManager(xml.NameTable);

            //    XElement customHeaders = xml.XPathSelectElements(@"/system.webServer/httpProtocol/customHeaders").SingleOrDefault();
            //    if (customHeaders != null)
            //    {
            //        XElement ele = XElement.Parse("<remove name=\"X - Powered - By\" />");
            //        customHeaders.Add(ele);
            //    }

            //}

            //configuration.Save();

            //MessageBox.Show("");


            //using (ServerManager serverManager = new ServerManager())
            //{

            //    Configuration config = serverManager.GetWebConfiguration("AgilePoint");


            //    // adding attributes in httpruntime
            //    ConfigurationSection syswebhttpRuntime = config.GetSection("system.web/httpRuntime");
            //    syswebhttpRuntime.SetAttributeValue("enableVersionHeader", false);


            //    // Adding a customHeaders entry
            //    ConfigurationSection httpProtocolSection = config.GetSection("system.webServer/httpProtocol");
            //    ConfigurationElementCollection customHeadersCollection = httpProtocolSection.GetCollection("customHeaders");

            //    ConfigurationElementCollection elecoll = customHeadersCollection.GetCollection();

            //    foreach (ConfigurationElement ele in elecoll)
            //    {
            //        if (ele["name"].ToString() == @"X-Custom-Name")
            //        {

            //        }
            //    }

            //    ConfigurationElement addElement = customHeadersCollection.CreateElement("add");
            //    addElement["name"] = @"X-Custom-Name";
            //    addElement["value"] = @"MyCustomValue";
            //    customHeadersCollection.Add(addElement);

            //    serverManager.CommitChanges();
            //}




            //ConfigurationSectionGroupCollection httpProtocol = configuration.SectionGroups;

            //string sectionName = "consoleSection";

            //ConfigurationSection customSection = (ConfigurationSection)configuration.GetSection(sectionName);

            //if (customSection == null)
            //{
            //    //customSection = new ConfigurationSection;

            //    //customSection = ConsoleSection();


            //    configuration.Sections.Add(sectionName, customSection);
            //}
            //else
            //    // Change the section configuration values.
            //    customSection = (ConfigurationSection)configuration.GetSection(sectionName);

            //// Save the configuration file.
            //configuration.Save(ConfigurationSaveMode.Modified);

            //// Force a reload of the changed section. This 
            //// makes the new values available for reading.
            //ConfigurationManager.RefreshSection(sectionName);

            ////
            ////Reading cdata section
            ////

            //templateConfig = TemplateFilePath.Text.Trim();// Template file path
            //actualConfig = ActualFilePath.Text.Trim(); // Portal config file path

            //XmlTextReader templateConfigReader = new XmlTextReader(templateConfig);
            //templateConfigReader.WhitespaceHandling = WhitespaceHandling.None;
            //XDocument templateDoc = XDocument.Load(templateConfigReader);

            //XmlTextReader actualConfigReader = null;
            //XDocument actualDoc = null;

            //IEnumerable<XElement> tempElementList = templateDoc.XPathSelectElements(@"/configurationTemplate/configurationItemTemplate");

            //foreach (XElement tempEle in tempElementList)
            //{
            //    XElement selectedElement;
            //    //XElement cData = tempEle.XPathSelectElement(@"CDataValue");
            //    string xpath = tempEle.Attribute("xpath").Value;
            //    bool isOverwrite = Convert.ToBoolean(tempEle.Attribute("overwrite").Value);
            //    string module = tempEle.Attribute("module").Value;

            //    XElement doc = XElement.Parse(tempEle.Value);

            //    if(module.ToLower() == "portal")
            //    {
            //        //actualConfig = portal web.config file path
            //    }
            //    else if(module.ToLower() == "analytics")
            //    {
            //        // actualConfig = analytics web.config file path
            //    }


            //    actualConfigReader = new XmlTextReader(actualConfig);
            //    actualConfigReader.WhitespaceHandling = WhitespaceHandling.None;
            //    actualDoc = XDocument.Load(actualConfigReader);

            //    XmlNamespaceManager nsManager = new XmlNamespaceManager(actualConfigReader.NameTable);
            //    nsManager.AddNamespace("prefix", "urn:schemas-microsoft-com:asm.v1");

            //    if (xpath.Contains("runtime"))
            //    {
            //        xpath = string.Format(xpath, "prefix");
            //    }

            //    selectedElement = actualDoc.XPathSelectElement(xpath.Trim(), nsManager);

            //    if (selectedElement != null)
            //    {
            //        if (isOverwrite)
            //            selectedElement.ReplaceNodes(doc.Elements());
            //    }
            //    //else
            //    //{
            //        // v8 Su2 Release: We are not handling this 'else' section as of now, since we do not confront this scenario as a regular case.
            //        // This section can be handle in future release if required,
            //        // For now any ubnormal case has to be handeled in a special manner.
            //    //}

            //    if (actualConfigReader != null)
            //    {
            //        actualConfigReader.Close();
            //        actualConfigReader = null;
            //    }
            //}

            //if (templateConfigReader != null)
            //{
            //    templateConfigReader.Close();
            //    templateConfigReader = null;
            //}

            //actualDoc.Save(actualConfig, SaveOptions.OmitDuplicateNamespaces);
        }

        public string CreatePath(XmlNode Node)
        {
            string Path = "/" + Node.Name;

            while (!(Node.ParentNode.Name == "#document"))
            {
                Path = "/" + Node.ParentNode.Name + Path;
                Node = Node.ParentNode;
            }
            Path = "/" + Path;
            return Path;
        }


        public Version GetIisVersion()
        {
            using (RegistryKey componentsKey = Registry.LocalMachine.OpenSubKey(@"Software\Microsoft\InetStp", false))
            {
                if (componentsKey != null)
                {
                    int majorVersion = (int)componentsKey.GetValue("MajorVersion", -1);
                    int minorVersion = (int)componentsKey.GetValue("MinorVersion", -1);

                    if (majorVersion != -1 && minorVersion != -1)
                    {
                        return new Version(majorVersion, minorVersion);
                    }
                }

                return new Version(0, 0);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //Version ver = GetIisVersion();

            //MessageBox.Show("IIS major version: " + ver.Major.ToString() + Environment.NewLine + "IIS minor version: " + ver.Minor.ToString());

            IsIIS6orAboveInstalled();
        }


        public static bool IsIIS6orAboveInstalled()
        {
            bool found = false;
            int regValue = 0;

            if (GetRegistryValue(RegistryHive.LocalMachine, "Software\\Microsoft\\InetStp", "MajorVersion", RegistryValueKind.DWord, out regValue))
            {
                //IISVersion = regValue;
                if (regValue >= 6)
                {
                    found = true;
                }
            }

            return found;
        }

        private static bool GetRegistryValue<T>(RegistryHive hive, string key, string value, RegistryValueKind kind, out T data)
        {
            bool success = false;
            data = default(T);

            using (RegistryKey baseKey = RegistryKey.OpenRemoteBaseKey(hive, String.Empty))
            {
                if (baseKey != null)
                {
                    using (RegistryKey registryKey = baseKey.OpenSubKey(key, RegistryKeyPermissionCheck.ReadSubTree))
                    {
                        if (registryKey != null)
                        {
                            try
                            {
                                // If the key was opened, try to retrieve the value.
                                RegistryValueKind kindFound = registryKey.GetValueKind(value);
                                if (kindFound == kind)
                                {
                                    object regValue = registryKey.GetValue(value, null);
                                    if (regValue != null)
                                    {
                                        data = (T)Convert.ChangeType(regValue, typeof(T), CultureInfo.InvariantCulture);
                                        success = true;
                                    }
                                }
                            }
                            catch (IOException Ioex)
                            {
                                //Logger.WriteErrorMessage(Ioex);
                                // The registry value doesn't exist. Since the
                                // value doesn't exist we have to assume that
                                // the component isn't installed and return
                                // false and leave the data param as the
                                // default value.
                            }
                            catch (Exception ex)
                            {
                                //Logger.WriteErrorMessage(ex);

                            }
                        }
                    }
                }
            }
            return success;
        }

        public static void UpdateCustomHeaders(string templateConfigPath, string portalConfigPath)
        {
            XmlDocument portalTempalteConfig = new XmlDocument();
            portalTempalteConfig.Load(@"C:\Users\ramkumar.bv\Desktop\ConfigMergingTesting\web.template.config");
            XmlNamespaceManager xmlNamespaceManager1 = new XmlNamespaceManager(portalTempalteConfig.NameTable);

            XmlNode templateCustomHeaders = portalTempalteConfig.DocumentElement.SelectSingleNode(@"/configuration/system.webServer/httpProtocol/customHeaders", xmlNamespaceManager1);

            XmlDocument portalConfig = new XmlDocument();
            portalConfig.Load(@"C:\Users\ramkumar.bv\Desktop\ConfigMergingTesting\v8.0-web.config");
            XmlNamespaceManager xmlNamespaceManager2 = new XmlNamespaceManager(portalConfig.NameTable);

            XmlNode customHeaders = portalConfig.DocumentElement.SelectSingleNode(@"/configuration/system.webServer/httpProtocol/customHeaders", xmlNamespaceManager2);
            if (customHeaders != null)
            {
                foreach (XmlNode chNode in customHeaders.ChildNodes)
                {
                    XmlNode xnd = templateCustomHeaders.SelectSingleNode(string.Format(@"{0}[@{1}='{2}']", chNode.Name, chNode.Attributes[0].Name, chNode.Attributes[0].Value));
                    if (xnd == null)
                    {
                        XmlNode newNode = portalTempalteConfig.CreateNode(XmlNodeType.Element, chNode.Name, String.Empty);

                        foreach (XmlAttribute attr in chNode.Attributes)
                        {
                            XmlAttribute newAtt = portalTempalteConfig.CreateAttribute(attr.Name);
                            newAtt.Value = attr.Value;
                            newNode.Attributes.Append(newAtt);
                        }

                        templateCustomHeaders.AppendChild(newNode);
                    }
                }
            }
        }
    }

    public static class XExtensions
    {
        /// <summary>
        /// Get the absolute XPath to a given XElement
        /// like /configuration/system.webServer/staticContent
        /// </summary>
        public static string GetAbsoluteXPath(this XElement element)
        {
            if (element == null)
            {
                throw new ArgumentNullException("element");
            }

            Func<XElement, string> relativeXPath = e =>
            {
                string name = e.Name.LocalName;

                XAttribute attribute = e.FirstAttribute;
                string attName, attValue, firstAttribute = string.Empty;

                if (attribute != null && attribute.Name.LocalName.ToLower() != "xmlns")
                {
                    attName = attribute.Name.LocalName;
                    attValue = attribute.Value;
                    firstAttribute = string.Format("[@{0}='{1}']", attName, attValue);
                }

                string xmlNameSpace = e.GetDefaultNamespace().ToString();
                string nsPrefix = string.Empty;

                if (!string.IsNullOrEmpty(xmlNameSpace))
                {
                    nsPrefix = "prefix:";
                }

                // Root node doesnt have parent
                return (element.Parent == null) ? "/" + name : string.Format("/{0}{1}{2}", nsPrefix, name, firstAttribute);
            };

            var ancestors = from e in element.Ancestors()
                            select relativeXPath(e);

            return string.Concat(ancestors.Reverse().ToArray()) +
                   relativeXPath(element);
        }
    }
}
